<?php if ($_SERVER["REQUEST_METHOD"] == "POST") {
//Checks for error messages and sets them if needed
    if (empty($_POST["Name"])) {
        $nameErr = "Name is required";
        $error = true;
    } else {
        $name = Functions::test_input($_POST["Name"]);
    }

    if (empty($_POST["SKU"])) {
        $skuErr = "SKU is required";
        $error = true;
    } else {
        $sku = Functions::test_input($_POST["SKU"]);
    }

    if (empty($_POST["Price"])) {
        $priceErr = "Price is required";
        $error = true;
    } else {
        if (is_numeric($_POST["Price"])) {
            $price = Functions::test_input($_POST["Price"]);
        } else {
            $priceErr = "Price should only contain numbers";
            $error = true;
        }
    }

    if (empty($_POST["Types"])) {
        $typeErr = "Type is required";
        $error = true;
    } else {
        $type = Functions::test_input($_POST["Types"]);
    }

    $valuesBasedOnType = Functions::findValuesBasedOnType($_POST, $type);
    foreach ($valuesBasedOnType as $value) {
        if (empty($value)) {
            $customErr = "Values are required";
            $error = true;
        } else {
            $value = Functions::test_input($value);
        }
    }
//Insert into database
    $insertErr = '';
    if ($error != true) {
        $product = new $_POST['Types']('', '', '');
        $product->setSKU($_POST['SKU']);
        $product->setName($_POST['Name']);
        $product->setPrice($_POST['Price']);
        foreach ($valuesBasedOnType as $key => $val) {
            $function = 'set' . $key;
            if (method_exists($product, $function)) {
                $product->$function($val);
            }
        }
        $insertErr = $product->insert();

    }
    if (!empty($insertErr)) {
        if (strpos($insertErr, 'SKU_UNIQUE') !== false) {
            $skuErr = "SKU is already taken";
        }
    } else {
        $insert = true;
    }

    $conn = null;

}