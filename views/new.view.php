<?php
foreach ($values as $v):
    $attributes = Database::getAttributes($v);
    $description = Database::getDescription($v);
    ?>
    <div id="custom<?= "{$v}" ?>" class="hidden customForm">
        <?php foreach ($attributes as $a): ?>
            <div class="formField ">
                <label for="<?= "{$v}-{$a}" ?>"><?= "{$a}" ?></label>
                <?php if (array_key_exists($a, $valuesBasedOnType)) {
                    echo "<input value = \"$valuesBasedOnType[$a]\" type = 'text' name = \"{$v}-{$a}\" id = \"{$v}-{$a}\" >";
                } else {
                    echo "<input value = \"\" type = \"text\" name = \"{$v}-{$a}\" id = \"{$v}-{$a}\" >";
                }
                ?>
            </div>
        <?php endforeach; ?>
        <p><?=$description?></p>
    </div id="AAA">

<?php endforeach; ?>

<span class="error"><?= $customErr ?></span>
</div>

