var value;

function changeForm(selectObject) {
    value = selectObject.value;
    var elements = document.querySelectorAll('.customForm');
    elements.forEach(checkForm);
}

function checkForm(item) {
    var children = item.querySelectorAll("input");
    var makeRequired;

    if (item.id === 'custom' + value) {
        item.className = "customForm";
        makeRequired = true;
    } else {
        item.className = "hidden customForm";
        makeRequired = false;
    }

    changeRequirement(children, makeRequired);
}

function changeRequirement(input, requirement) {
    input.forEach(function (element) {
            element.required = requirement;
        }
    );
}

changeForm(document.getElementById('TypeSwitcher'));