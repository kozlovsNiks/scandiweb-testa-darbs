<?php
class Functions
{
    public static function str_replace_first($search, $replace, $subject) {
        $pos = strpos($subject, $search);
        if ($pos !== false) {
            return substr_replace($subject, $replace, $pos, strlen($search));
        }
        return $subject;
    }

    public static function findValuesBasedOnType(array $arr, $type)
    {
        $values = array();
        foreach ($arr as $key => $value) {
            if (substr($key, 0, strlen($type)) === $type) {
                $values += [str_replace($type . '-',"",$key) => $value];
            }
        }

        return $values;
    }

    //Removes special characters
    public static function test_input($data) {
        $data = trim($data);
        $data = stripslashes($data);
        $data = htmlspecialchars($data);
        return $data;
    }
}