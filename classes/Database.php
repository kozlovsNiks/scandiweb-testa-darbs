<?php
class Database
{
    public static function getProducts()
    {
        try {
            $stm = self::conn()->prepare('call getProducts()');
            $stm->execute();
            $products = $stm->fetchAll(PDO::FETCH_ASSOC);
            return $products;
        } catch (PDOException $e) {
            var_dump($e);
            return '';
        }
    }

    public static function conn()
    {
        $server = 'localhost';
        $dbName = 'scandiweb';
        $user = 'root';
        $pass = '';
        $conn = NULL;
        if ($conn == NULL) {
            try {
                $conn = new PDO('mysql:host=' . $server . ';dbname=' . $dbName, $user, $pass, array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8"));
                $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            } catch (PDOException $e) {
                echo 'PDO Error: ' . $e->getMessage();
            }
        }
        return $conn;
    }

    public static function getValues($sku) {
        try {
            $stm = self::conn()->prepare('call getValues(:sku)');
            $stm->execute(array(':sku' => $sku));
            $values = $stm->fetchAll(PDO::FETCH_KEY_PAIR);
            return $values;
        } catch (PDOException $e) {
            var_dump($e);
            return '';
        }
    }

    public static function getOptions()
    {
        try {
            $stm = self::conn()->prepare('Select `Type` from `types`;');
            $stm->execute();
            $values = $stm->fetchAll(PDO::FETCH_COLUMN, 0);

            return $values;
        } catch (PDOException $e) {
            var_dump($e);
            return '';
        }
    }

    public static function getAttributes($type)
    {
        try{
        $stm = self::conn()->prepare('Select `value` from attributes where `Type` = :type');
        $stm->execute(array(':type' => $type));
        $types = $stm->fetchAll(PDO::FETCH_COLUMN, 0);

        return $types;
        } catch (PDOException $e) {
            var_dump($e);
            return '';
        }
    }

    public static function getDescription($type)
    {
        try {

            $stm = self::conn()->prepare('Select `Description` from `types` where `Type` = :type');
            $stm->execute(array(':type' => $type));
            $description = $stm->fetch(PDO::FETCH_COLUMN, 0);
            return $description;
        } catch (PDOException $e) {
            var_dump($e);
            return '';
        }
    }

}