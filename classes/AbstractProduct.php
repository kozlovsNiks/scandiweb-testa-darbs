<?php


abstract class AbstractProduct
{
    private $sku, $name, $price;


    public function __construct($inSku, $inName, $inPrice)
    {
        if ($inSku != '' & $inName != '' & $inPrice != '') {
            $this->setSku($inSku);
            $this->setName($inName);
            $this->setPrice($inPrice);
        }
    }

    abstract public function setUpValues();

    /**
     * @param mixed $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @param mixed $price
     */
    public function setPrice($price)
    {
        $this->price = $price;
    }

    public function delete()
    {
        try {
            $stm = Database::conn()->prepare('call deleteProduct(:sku)');
            $stm->execute(array(':sku' => $this->getSku()));
        }catch (PDOException $e) {
            return $e;
        }
    }

    /**
     * @return mixed
     */
    public function getSku()
    {
        return $this->sku;
    }

    /**
     * @param mixed $sku
     */
    public function setSku($sku)
    {
        $this->sku = $sku;
    }

    public function insert()
    {
        try {
            $conn = Database::conn();
            $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            $sql = 'INSERT INTO product (sku, `name`, `type`, price) values (:sku,:name,:type,:price)';
            $stm = $conn->prepare($sql);
            $stm->execute(array(':sku' => $this->getSku(), ':name' => $this->getName(), ':type' => get_class($this), ':price' => $this->getPrice()));
            //For is empty check
            return 0;
        } catch (PDOException $e) {
            return $e;
        }

    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @return mixed
     */
    public function getPrice()
    {
        return $this->price;
    }

    public function printContainer()
    {
        echo '<div class="product">
<input form="form" name="skus[]" type="checkbox" class="product__checkbox" value ="' . $this->getSku() . '">
<p>' . $this->getSku() . '</p>
<p>' . $this->getName() . '</p>
<p>' . $this->getPrice() . ' $</p>
<p>' . $this->getValue() . '</p>
</div>';
    }

    abstract public function getValue();
}