<?php


class Book extends AbstractProduct
{
    private $weight;

    public function __construct($inSku, $inName, $inPrice)
    {
        if ($inSku != '' & $inName != '' & $inPrice != '') {
            parent::__construct($inSku, $inName, $inPrice);
            $this->setUpValues();
        }
    }

    public function setUpValues()
    {
        if ($this->getSku() != '' & $this->getName() != '' & $this->getPrice() != '') {
            $this->setWeight(Database::getValues($this->getSku())['Weight']);
        }
    }

    public function insert()
    {
        $parentErr = parent::insert();
        if (!empty($parentErr)) {
            return $parentErr;
        }
        try {
            $conn = Database::conn();
            $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            $sql = 'call insertValues(:sku,:value,:attribute)';
            $stm = $conn->prepare($sql);
            $stm->execute(array(':sku' => $this->getSku(), ':value' => $this->getWeight(),':attribute'=> 'Weight'));
            //For is empty check
            return 0;
        } catch (PDOException $e) {
            $this->delete();
            return $e;
        }

    }

    public function getValue()
    {
        return 'Weight: ' . $this->getWeight() . ' KG';
    }

    /**
     * @return mixed
     */
    public function getWeight()
    {
        return $this->weight;
    }

    /**
     * @param mixed $weight
     */
    public function setWeight($weight): void
    {
        $this->weight = $weight;
    }

}