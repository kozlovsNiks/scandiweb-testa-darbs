<?php

//Blank product for storing skus and calling delete functions
class BlankProduct extends AbstractProduct
{
    public function __construct($inSku)
    {
        $this->setSku($inSku);
    }

    function setUpValues()
    {
        //Does nothing because not needed
    }
    public function getValue()
    {
        return '';
    }
}