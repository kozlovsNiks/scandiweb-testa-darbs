<?php


class Disc extends AbstractProduct
{
    private $size;

    /**
     * @param $inSku
     * @param $inName
     * @param $inPrice
     */
    public function __construct($inSku,$inName, $inPrice)
    {
        if ($inSku != '' & $inName != '' & $inPrice != '') {
            parent::__construct($inSku, $inName, $inPrice);
            $this->setUpValues();
        }
    }

    public function setUpValues()
    {
        if ($this->getSku() != '' & $this->getName() != '' & $this->getPrice() != '') {
            $this->setSize(Database::getValues($this->getSku())['Size']);
        }
    }

    public function getValue()
    {
        return 'Size: ' . $this->getSize() . ' MB';
    }

    public function getSize()
    {
        return $this->size;
    }

    /**
     * @param mixed $size
     */
    public function setSize($size): void
    {
        $this->size = $size;
    }

    public function insert()
    {
        $parentErr = parent::insert();
        if (!empty($parentErr)) {
            return $parentErr;
        }
        try {
            $conn = Database::conn();
            $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            $sql = 'call insertValues(:sku,:value,:attribute)';
            $stm = $conn->prepare($sql);
            $stm->execute(array(':sku' => $this->getSku(), ':value' => $this->getSize(),':attribute' => 'Size'));
            //For is empty check
            return 0;
        } catch (PDOException $e) {
            $this->delete();
            return $e;
        }
    }

}