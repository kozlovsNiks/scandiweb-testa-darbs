<?php


class Furniture extends AbstractProduct
{
    private $height, $width, $length;

    /**
     * @param $inSku
     * @param $inName
     * @param $inPrice
     */
    public function __construct($inSku, $inName, $inPrice)
    {
        if ($inSku != '' & $inName != '' & $inPrice != '') {
            parent::__construct($inSku, $inName, $inPrice);
            $this->setUpValues();
        }
    }
    public function setUpValues()
    {
        if ($this->getSku() != '' & $this->getName() != '' & $this->getPrice() != '') {
            $values = Database::getValues($this->getSku());
            $this->setHeight($values['Height']);
            $this->setLength($values['Length']);
            $this->setWidth($values['Width']);
        }
    }

    /**
     * @param mixed $length
     */
    public function setLength($length): void
    {
        $this->length = $length;
    }

    /**
     * @param mixed $width
     */
    public function setWidth($width): void
    {
        $this->width = $width;
    }

    public function insert()
    {
        $parentErr = parent::insert();
        if (!empty($parentErr)) {
            return $parentErr;
        }
        try {
            $conn = Database::conn();
            $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            $sql = 'call insertValues(:sku,:value,:attribute)';
            $stm = $conn->prepare($sql);
            $stm->execute(array(':sku' => $this->getSku(), ':value' => $this->getHeight(),':attribute' => 'Height'));
            $stm->execute(array(':sku' => $this->getSku(), ':value' => $this->getLength(),':attribute' => 'Length'));
            $stm->execute(array(':sku' => $this->getSku(), ':value' => $this->getWidth(), ':attribute' => 'Width'));
            //For is empty check
            return 0;
        } catch (PDOException $e) {
            $this->delete();
            return $e;
        }

    }

    public function printContainer()
    {
        echo '<div class="product">
<input form="form" name="skus[]" type="checkbox" class="product__checkbox" value ="' . $this->getSku() . '">
<p>' . $this->getSku() . '</p>
<p>' . $this->getName() . '</p>
<p>' . $this->getPrice() . '</p>
<p>' . $this->getValue() . '</p>
</div>';
    }

    /**
     * @return mixed
     */
    public function getValue()
    {
        return 'Dimension: ' . $this->getHeight() . 'x' . $this->getWidth() . 'x' . $this->getLength();
    }

    public function getHeight()
    {
        return $this->height;
    }

    /**
     * @param mixed $height
     */
    public function setHeight($height): void
    {
        $this->height = $height;
    }

    /**
     * @return mixed
     */
    public function getWidth()
    {
        return $this->width;
    }

    /**
     * @return mixed
     */
    public function getLength()
    {
        return $this->length;
    }

}