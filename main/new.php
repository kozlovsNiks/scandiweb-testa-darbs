<?php require_once __DIR__ . '/../vendor/autoload.php';?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Product add</title>
    <link rel="stylesheet" type="text/css" href="../css/new.css">
</head>
<body>
<?php
//Initialization
$nameErr = $skuErr = $priceErr = $typeErr = $customErr = "";
$name = $sku = $price = $type = "";
$valuesBasedOnType = array();
$error = false;
$insert = false;

require '../views/newPostCheck.view.php';


?>

<nav>
    <h1>Product list</h1>
    <input type="submit" form="form" value="Save">
</nav>
<hr>
<main>
    <form id="form" method="post" action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]); ?>">
        <div class="formField">
            <label for="SKU">SKU </label>
            <input value="<?= $sku; ?>" type="text" name="SKU" id="SKU" required>
        </div>
        <span class="error"><?= $skuErr ?></span>
        <div class="formField">
            <label for="Name">Name </label>
            <input value="<?= $name; ?>" type="text" name="Name" id="Name" required>

        </div>
        <span class="error"><?= $nameErr ?></span>

        <div class="formField">
            <label for="Price">Price </label>
            <input value="<?= $price ?>" type="text" name="Price" id="Price" required>
        </div>
        <span class="error"><?= $priceErr ?></span>

        <div class="formField">
            <label for="TypeSwitcher">
                Type Switcher
            </label>

            <select name="Types" id="TypeSwitcher" required onchange="changeForm(this)">
                <option value="">Type Switcher</option>
                <?php
                $values = Database::getOptions();
                //Listbox gets selected values
                foreach ($values as $v) {
                    if ($v == $type) {
                        echo "<option selected value=\"{$v}\">{$v}</option>";
                    } else {
                        echo "<option value=\"{$v}\">{$v}</option>";
                    }
                }
                ?>
            </select>
        </div>
        <span class="error"><?= $typeErr ?></span>

        <?php
        require '../views/new.view.php';
        ?>

    </form>
    <?php
    if ($_SERVER["REQUEST_METHOD"] == "POST") {
        if ($insert == true) {
            echo '<h1 style="color:green; text-align: center">Insert succesful</h1>';
        } else {
            echo '<h1 style="color:red; text-align: center">Insert failed</h1>';
        }
    }
    ?>
</main>
<script type="text/javascript" src="../js/forms.js"></script>
</body>
</html>