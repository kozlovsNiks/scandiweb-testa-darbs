

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Product list</title>
    <link rel="stylesheet" type="text/css" href="../css/list.css">
</head>
<body>
<?php
require_once __DIR__ . '/../vendor/autoload.php';
$products = array();
$errMessage = "";
//Deletes selected products from DB
if ($_SERVER["REQUEST_METHOD"] == "POST") {
    if (empty($_POST["skus"])) {
        $errMessage = "Nothing selected to delete";
    } else {
        foreach ($_POST["skus"] as $v) {
            $v = htmlspecialchars($v);
            $product = new BlankProduct($v);
            $errMessage = $product->delete();
        }
    }
}
?>
<nav>
    <h1>Product list</h1>
    <span><?= $errMessage ?></span>
    <form id="form" method="post" action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]); ?>">
        <div class="mass-delete">
            <select id="options">
                <option value="mass_delete">Mass delete</option>
            </select>
            <input type="submit" value="Apply" form="form">
        </div>
    </form>

</nav>
<hr>
<main>
    <?php require('../views/listProducts.view.php'); ?>
</main>
</body>
</html>