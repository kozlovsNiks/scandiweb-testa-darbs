-- phpMyAdmin SQL Dump
-- version 4.8.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1:3306
-- Generation Time: May 08, 2019 at 08:09 PM
-- Server version: 5.7.24
-- PHP Version: 7.2.14

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `scandiweb`
--

DELIMITER $$
--
-- Procedures
--
DROP PROCEDURE IF EXISTS `deleteProduct`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `deleteProduct` (IN `sku` VARCHAR(45))  BEGIN
	DELETE FROM `product` WHERE (product.SKU = sku);
    DELETE FROM `product_values` WHERE (product_values.ProductSKU = sku);
END$$

DROP PROCEDURE IF EXISTS `getProducts`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `getProducts` ()  BEGIN
	Select SKU as sku, `Name` as `name`, Price as price,product.`Type` as `type` from product inner join `types` on product.`Type` = `types`.`type`;
END$$

DROP PROCEDURE IF EXISTS `getValues`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `getValues` (IN `sku` VARCHAR(45))  BEGIN
	Select attributes.`Value`,product_values.`Value` from product_values inner join attributes on AttributeID = attributes.ID where ProductSKU = sku;
END$$

DROP PROCEDURE IF EXISTS `insertValues`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `insertValues` (IN `sku` VARCHAR(45), IN `val` VARCHAR(45), IN `attribute` VARCHAR(45))  BEGIN
declare attributeID int;
	Set attributeID = (Select attributes.ID from product inner join attributes on product.`Type` = attributes.`Type` where product.SKU = sku and attributes.`Value` = attribute);
    insert into product_values (ProductSKU,AttributeID,`Value`) values (sku,attributeID,val);
END$$

DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `attributes`
--

DROP TABLE IF EXISTS `attributes`;
CREATE TABLE IF NOT EXISTS `attributes` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `Type` varchar(45) COLLATE utf8_unicode_ci NOT NULL,
  `Value` varchar(45) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=MyISAM AUTO_INCREMENT=6 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `attributes`
--

INSERT INTO `attributes` (`ID`, `Type`, `Value`) VALUES
(1, 'Disc', 'Size'),
(2, 'Book', 'Weight'),
(3, 'Furniture', 'Height'),
(4, 'Furniture', 'Width'),
(5, 'Furniture', 'Length');

-- --------------------------------------------------------

--
-- Table structure for table `product`
--

DROP TABLE IF EXISTS `product`;
CREATE TABLE IF NOT EXISTS `product` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `SKU` varchar(45) COLLATE utf8_unicode_ci NOT NULL,
  `Name` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  `Type` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  `Price` decimal(13,2) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `SKU_UNIQUE` (`SKU`)
) ENGINE=MyISAM AUTO_INCREMENT=31 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `product`
--

INSERT INTO `product` (`ID`, `SKU`, `Name`, `Type`, `Price`) VALUES
(1, 'JVC200123', 'Acme Disc', 'Disc', '1.00'),
(19, 'Product1', 'The story about me', 'Book', '99.99'),
(20, 'Product2', 'Gaming chair', 'Furniture', '399.99'),
(21, 'Product3', 'RW disk', 'Disc', '9.99');

-- --------------------------------------------------------

--
-- Table structure for table `product_values`
--

DROP TABLE IF EXISTS `product_values`;
CREATE TABLE IF NOT EXISTS `product_values` (
  `PK` int(11) NOT NULL AUTO_INCREMENT,
  `ProductSKU` varchar(45) COLLATE utf8_unicode_ci NOT NULL,
  `AttributeID` int(11) NOT NULL,
  `Value` varchar(45) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`PK`)
) ENGINE=MyISAM AUTO_INCREMENT=88 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `product_values`
--

INSERT INTO `product_values` (`PK`, `ProductSKU`, `AttributeID`, `Value`) VALUES
(1, 'JVC200123', 1, '700'),
(15, 'Product1', 2, '1'),
(83, 'HHHa', 2, '5'),
(18, 'Product2', 5, '44'),
(17, 'Product2', 4, '24'),
(16, 'Product2', 3, '12'),
(19, 'Product3', 1, '500'),
(84, 'HHHb', 1, '6');

-- --------------------------------------------------------

--
-- Table structure for table `types`
--

DROP TABLE IF EXISTS `types`;
CREATE TABLE IF NOT EXISTS `types` (
  `Type` varchar(45) COLLATE utf8_unicode_ci NOT NULL,
  `Description` text COLLATE utf8_unicode_ci COMMENT 'Description for the product add page for each custom type',
  PRIMARY KEY (`Type`),
  UNIQUE KEY `Type_UNIQUE` (`Type`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `types`
--

INSERT INTO `types` (`Type`, `Description`) VALUES
('Furniture', 'Please provide dimensions in HxWxL format'),
('Disc', 'Please provide storage size in MB'),
('Book', 'Please provide weight of book in KG');
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
